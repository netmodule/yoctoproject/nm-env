# aarch64-netmodule-linux
SRCREV_pn-linux-netmodule_mx8 = "f9f46b770da2d542ca9370b7f899170587f0f19c"
# cortexa8t2hf-neon-netmodule-linux-gnueabi
SRCREV_pn-linux-netmodule_ti33x = "511cc2e17069fe1075e69c47dbb1e12cbc49b90b"
# cortexa9t2hf-neon-netmodule-linux-gnueabi
SRCREV_pn-linux-netmodule_armada = "511cc2e17069fe1075e69c47dbb1e12cbc49b90b"
# bootloader and similar parts
SRCREV_pn-u-boot-ti33x = "7b4add1789640d188d2c5d4c7885f942908846f6"
SRCREV_pn-u-boot-imx8-nmhw23 = "68ad3ed09773011ab15e9dd50f9ec2bbf817ef3f"
SRCREV_pn-u-boot-armada = "eda2a82ae7d32d2611d6c7280ca8a347fe00b8bc"
SRCREV_pn-imx-boot = "dd0234001713623c79be92b60fa88bc07b07f24f"
SRCREV_pn-imx-atf = "413e93e10ee4838e9a68b190f1468722f6385e0e"
SRCREV_pn-bootloader-config = "60269b7bbb4076247607c7a59b2134c3a5369f07"
# common parts
SRCREV_aktualizr_pn-aktualizr-native = "13800e0cfda285485e632f0a2d50490ba88d0f72"
SRCREV_pn-gnss-mgr = "e7970e6486473c78c2c6f1f63c5a15fc28606c2d"
SRCREV_pn-libnmapp = "17508c33a5435a8d680c63052960ad5ca19d5b3c"
SRCREV_pn-modemmanager = "35ef2f387bf53f0601901a5f08ab0f6bf57105c4"
SRCREV_pn-nmhw-fwupdate = "68b84ce457e8cd4a59e2f366114edf0098acbe4d"
SRCREV_pn-nmubxlib = "5ccd0fe246be8f381c9b06e2703b6fe22fefcee8"
SRCREV_pn-ssc-broker-driver = "4950e67e4a94bde44f8cf7a8c3f0c8eed8d18ad9"
SRCREV_pn-ssc-sysstate-driver = "19a75e6acbb5541c905ecdfc63ddba1591a619bf"
SRCREV_pn-ssf-mgr = "f38b4845464ed7f56cd371fec9bcf9b139077947"
SRCREV_pn-storage-info = "761b6e2de7e697e44f6d37a4cafc98e72155340b"
SRCREV_pn-sys-mon = "495c4e4364af589c04a1ff52b048615c9d2e931c"
SRCREV_pn-udev-rules-nmhw = "18d65c29b6cf36f3304c2883a6cda7ba3d2b2a3e"
SRCREV_pn-wlconf-bin = "cf9bfca0a80f02eb4a4245ab0144f8e956e24b95"
SRCREV_pn-wwan-config = "e75720143a3863bb0df8e7f40be56cf1976e6c96"
